class OrganizationsUsers < ActiveRecord::Migration
  def change
  	create_table :organizations_users do |t|
      t.references :organization, index: true
      t.references :user, index: true

      t.timestamps null: false
      
    end
    add_foreign_key :organizations_users, :organizations
    add_foreign_key :organizations_users, :users
    add_index :organizations_users, [:organization_id, :user_id], unique: true
  end
end
